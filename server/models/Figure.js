const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const handleError = require('../helpers').handleError;

const FigureSchema = new Schema({
  id: ObjectId,
  fullPath: String,
  parents: [],
  children: {
    fields: [],
    tags: [],
    thumbnails: []
  },
  order: Number,
  isTouched: {
    type: Boolean,
    default: false
  },
  createdOn: {
    type: Number,
    default: new Date().getTime()
  },
  lastUpdatedOn: {
    type: Number,
    default: new Date().getTime()
  }
});

module.exports = mongoose.model('Figure', FigureSchema);
