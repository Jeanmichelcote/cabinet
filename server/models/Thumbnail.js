const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;
const handleError = require('../helpers').handleError;

const ThumbnailSchema = new Schema({
  id: ObjectId,
  fullPath: String,
  parents: [],
  createdOn: {
    type: Number,
    default: new Date().getTime()
  }
});

module.exports = mongoose.model('Thumbnail', ThumbnailSchema);
