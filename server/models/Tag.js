const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const TagSchema = new Schema({
  id: ObjectId,
  value: {
    type: String,
    required: true
  },
  label: String,
  parents: [],
  createdOn: {
    type: Number,
    default: new Date().getTime()
  },
  lastUpdatedOn: {
    type: Number,
    default: new Date().getTime()
  }
});

module.exports = mongoose.model('Tag', TagSchema);
