const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const FieldSchema = new Schema({
  id: ObjectId,
  label: {
    type: String,
    required: true
  },
  value: {
    type: String,
    required: true
  },
  parent: {
    type: ObjectId,
    required: true
  },
  order: Number,
  createdOn: {
    type: Number,
    default: new Date().getTime()
  },
  lastUpdatedOn: {
    type: Number,
    default: new Date().getTime()
  }
});

FieldSchema.query.byOwner = function(ownerId) {
  return this.find({ownerId: new RegExp(ownerId, 'i') });
};

module.exports = mongoose.model('Field', FieldSchema);
