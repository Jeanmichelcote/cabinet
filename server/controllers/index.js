const makeController = require('./makeController');
const create = require('./createMethods');

/* 
 * The makeController() function is a curried factory.
 * It takes the controller name as first argument 
 * and its "create" method as second argument, since all the 
 * other CRUD methods are pretty generic
 * 
 */

module.exports = {
  fields: makeController('fields')(create.field), 
  tags:  makeController('tags')(create.tag),
  projects: makeController('projects')(create.project),
  figures: makeController('figures')(create.figure)
}