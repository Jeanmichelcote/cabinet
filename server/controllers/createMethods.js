const db = require('../db');
const formatValues = require('../helpers').formatValues;
const trim = require('lodash').trim;
const toLower = require('lodash').toLower;

const Field = require('../models/Field');
const Tag = require('../models/Tag');
const Project = require('../models/Project');
const Figure = require('../models/Figure');

module.exports = {
  field: (body, cb) => {
    let validFields = [];
    let errors = [];

    body.forEach(fieldObject => {
      const {label, value, parent} = fieldObject;

      /* Stack errors */
      label || errors.push('Must provide a "label" property to the new field');
      value || errors.push('Must provide a "value" property to the new field');
      parent || errors.push('Must provide a "parent" property to the new field');

      /* Stack validated fields */
      validFields.push(new Field(formatValues(fieldObject, trim))); 
    });

    if (errors.length > 0) return cb({message: errors.join('\n')}, null);
   
    return db.get().collection('fields')
      .insertMany(validFields, (err = null, data = null) => cb(err, data.ops) );
  },  

  tag: (body, cb) => {
    let validTags = [];
    let error = null;    

    body.forEach(tagObject => {
      const {value} = tagObject;

      error = value ? null : 'Must provide a name for the new tag';
      validTags.push(new Tag(formatValues(tagObject, trim, toLower))); 
    });

    if (error) return cb({message: errors}, null);

    return db.get().collection('tags')
      .insertMany(validTags, (err = null, data = null) => cb(err, data.ops) );
  },

  project: (body, cb) => db
    .get().collection('projects')
    .insertOne(new Project, (err = null, data = null) => cb(err, data.ops[0]))
  ,

  figure: (files, cb) => {
    let errors = [];    

    body.forEach(figureObject => {
      const {location} = figureObject;

      value || errors.push('Must provide a name for the new tag');
      validTags.push(new Figure(formatValues(tagObject, trim))); 
    });

    if (errors.length > 0) return cb({message: errors.join('\n')}, null);
   
    return db.get().collection('figures')
    .insertMany(validFigures, (err = null, data = null) => cb(err, data.ops));
  }
};