const db = require('../db');
const ObjectID = require('mongodb').ObjectID;

/* Curried function */
/* All the CRUD actions can be generic except "create" */
/* Create has to be unique for every Model, so we need to pass it as argument */
module.exports = function(name) {
  return function(createMethod) {
    return {
      find: (params, cb) => {
        const target = params.parent
          ? {parent: new ObjectID(params.parent)}
          : {}
        db
          .get().collection(name)
          .find(target)
          .toArray((err = null, data = null) => cb(err, data)
        )},

      findById: (id, cb) => db
        .get().collection(name)
        .findOne({ _id: new ObjectID(id) }, (err = null, data = null) =>
          cb(err, data)
        ),

      create: (body, cb) => createMethod(body, cb),

      update: (id, body, cb) => db
        .get().collection(name)
        .updateOne({_id: new ObjectID(id)}, {$set: body}, (err = null, doc = null) => 
          cb(err, doc)
        ),

      destroy: (id, cb) => db
        .get().collection(name)
        .deleteOne({_id: new ObjectID(id)}, (err = null, data = null) => cb(err, data) )
    };
  };
};