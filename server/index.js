/* eslint consistent-return:0 */

const express = require('express');
const compression = require('compression');
const logger = require('logger').createLogger();
const bodyParser = require('body-parser');

const api = require('./api');
const db = require('./db');

const app = express();

// If you need a backend, e.g. an API, add your custom backend-specific middleware here
app.use(bodyParser.json());
app.use(compression());
app.use(express.static('../build'))
app.use('/api', api);

// get the intended host and port number, use localhost and port 3000 if not provided
const host = 'localhost';
const port = process.env.PORT || 3001;

// Express only serves static assets in production
if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'));
}

app.get('/*', function (req, res) {
   res.sendFile(path.join(__dirname, 'client/build', 'index.html'));
 });

// Start your app.
const dbUrl = 'mongodb://localhost:27017/cabinet';

db.connect(dbUrl, err => {
  if (err) {
    logger.error('Unable to connect. Mongo is probably not running.');
    process.exit(1);
  } else {
    logger.info(`Server now running at http://localhost:${port}`)
    app.listen(port, host, err => {
      err && logger.error(err.message);
    });

  }
});
