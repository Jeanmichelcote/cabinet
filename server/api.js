const express = require('express');
const router = express.Router();
const multer  = require('multer');
const controllers = require('./controllers');
const handleError = require('./helpers').handleError;

// GET all 
router.route('/:resource')
  /* Get all or by query */
  .get((req, res, next) => {
    const resource = req.params.resource;
    const query = req.query
    const controller = controllers[resource];

    !controller
      ? res.json({
          confirmation: 'fail',
          message: 'Invalid GET resource request: ' + resource
        })
      : controller.find(query, (err, results) => err
          ? handleError(res, err.message, "Failed to find fields")
          : res.status(200).json(results));
  })

  /* Create new resource */
  .post((req, res, next) => {
    const resource = req.params.resource;
    const body = req.body;
    const controller = controllers[resource];

    !controller
      ? res.json({
          confirmation: 'Fail',
          message: 'Invalid POST resource request: ' + resource
        })
      : controller.create(body, (err, result) => err
        ? handleError(res, err.message, "Failed to create new field") 
        : res.status(201).json(result));
  });

// GET by Id 
router.route('/:resource/:id')
  .get((req, res, next) => {
    const resource = req.params.resource;
    const id = req.params.id;
    const controller = controllers[resource];

    !controller
      ? res.json({
          confirmation: 'Fail',
          message: 'Invalid GET resource request: ' + resource
        })
      : controller.findById(id, (err, result) => err
        ? handleError(res, err.message, "Failed to find field") 
        : res.status(200).json(result));
  })

  /* PATCH by Id */
  .patch((req, res, next) => {
    const resource = req.params.resource;
    const id = req.params.id;
    const body = req.body;
    const controller = controllers[resource];

    !controller
      ? res.json({
          confirmation: 'Fail',
          message: 'Invalid PUT resource request: ' + resource
        })
      : controller.update(id, body, (err, result) => err
        ? handleError(res, err.message, "Failed to modify field") 
        : res.status(200).json(result));  
  })

  /* DELETE by Id */
  .delete((req, res, next) => {
    const resource = req.params.resource;
    const id = req.params.id;
    const controller = controllers[resource];

    !controller
      ? res.json({
          confirmation: 'Fail',
          message: 'Invalid DELETE resource request: ' + resource
        })
      : controller.destroy(id, (err, result) => err
        ? handleError(res, err.message, "Failed to delete field") 
        : res.status(200).json(result));
  });

// Handling file uploads
const storage = multer.diskStorage({
  destination: './uploads',
  filename(req, file, cb) {
    cb(null, `${new Date()}-${file.originalname}`);
  },
});

const upload = multer({storage});

router.route('/figures/upload')
  .post(upload.array('figures'), (req, res) => {
    const files = req.files;
    console.log('Upload files', files)
  });

module.exports = router;