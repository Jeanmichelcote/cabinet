const flow = require('lodash').flow;

// Generic error handler used by all endpoints.
exports.handleError = function(res, reason, message, code) {
  console.log("ERROR: " + reason);
  res.status(code || 500).json({
    confirmation: 'fail',
    message: message,
    reason: reason
  });
  //return message;
};

exports.formatValues = function(obj, ...formatters) {
  let newObject = {};
  const format = flow(...formatters);

  Object.keys(obj).forEach(key => 
    Object.assign(newObject, { [key]:format(obj[key]) })
  );

  return newObject;  
};
