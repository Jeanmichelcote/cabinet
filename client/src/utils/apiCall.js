import {request} from 'helpers'

/* API Calls */
export default function apiCall(endpoint) {
  return {
    getIt: (id, callback) => {
      const path = id ? `${endpoint}/${id}` : endpoint
      return request(path)
        .then(res => callback ? callback(res) : res)
    },

    createIt: (event, callback) => {
      event.preventDefault()
      const ressourceName = event.target.querySelector('input').value

      return request(endpoint, {method: 'POST'})
        .then(res =>
          callback ? callback(res, ressourceName) : {...res, ressourceName})
    },

    updateIt: (id, newData, callback) =>
      request(`${endpoint}/${id}`, {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: 'PATCH',
        body: JSON.stringify(newData)
      })
        .then(res => callback ? callback(res) : res)
    ,

    deleteIt: (id, callback) =>
      request(`${endpoint}/${id}`, {method: 'DELETE'})
        .then(() => request(`api/fields?parents=${id}`, {method: 'DELETE'}) )
          .then(res => callback ? callback(res) : res)
  }
}

//
// const ressource = apiCall(this.endpoint)
// ressource.getIt()
// ressource.createIt(event, (data, ressourceName) => 
//   request('api/fields', {
//     method: 'POST',
//     body: JSON.stringify({
//       label: 'Name',
//       value: ressourceName,
//       parent: data.id,
//     })
//   })
// )
// ressource.updateIt(id)
