import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Icon from 'react-icons/lib/go/x'

const Clear = styled.span`
  xbackground-color: #ddd;
  xpadding: 0 0.3rem;
  margin: 0 0 0 0.2rem;
  border-left: 1px solid #99b;
  xborder-radius: 2px;
  color: #99b;

  &:hover {
    color: darkred;
  }
`

const Dismiss = (props) => {
  const {handleClick, id, ...style} = props
  console.log('dismiss', props)
  return (
    <Clear>
      <Icon {...style} onClick={() => handleClick(id)} />
    </Clear>
  )
}

Dismiss.displayName = 'Dismiss'
Dismiss.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  color: PropTypes.string,
  handleClick: PropTypes.func
}

export default Dismiss;