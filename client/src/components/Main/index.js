import React from 'react'
import styled from 'styled-components'
import {Route, Switch} from 'react-router-dom'
import ProjectsPage from 'containers/ProjectsPage'
import LibraryPage from 'containers/LibraryPage'

// Styled
const MainFrame = styled.main`
  margin: 4em 0 3em 0;
  z-index: -10;
  width: 94%;
  height: 100%;
`

// Component
const Main = () => (
  <MainFrame>
    <Switch>
      <Route exact path="/" component={ProjectsPage}/>
      <Route path="/library" component={LibraryPage}/>
    </Switch>
  </MainFrame>
);


export default Main