import React from 'react'
import styled from 'styled-components'
import {BrowserRouter as Router} from 'react-router-dom'
import Header from 'components/Header'
import Footer from 'components/Footer'
import Main from 'components/Main'

// Styles
const Wrapper = styled.div`
  margin-left: 1rem;
`

const App = () => (
  <Router>
    <Wrapper>
      <Header />
      <Main />
      <Footer />
    </Wrapper>
  </Router>
);

export default App;
