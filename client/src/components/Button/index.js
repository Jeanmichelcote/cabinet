/**
*
* Button
*
*/

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

// Styles
const StyledButton = styled.button`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border: 1px solid #9a9a9a;
  border-radius: 3px;
  font-size: 0.8rem;
  cursor: ${props => props.disabled ? 'default' : 'pointer'};
  background-color: #88a;
  color: white;
  opacity: ${props => props.disabled ? 0.8 : 1}
  ${props => props.style}

  &:hover {
    opacity: 0.8;
  }
`;

const Label = styled.label`
  font-family: sans-serif;
  font-variant: small-caps;
  margin-right: ${props => props.label === "" ? 'none' : '1rem'};
`

// Stateless component
function Button({label="", onClick, style, disabled, children}) {
  return (
    <StyledButton onClick={onClick} style={style} disabled={disabled}>
      <Label label={label}>{label}</Label>
      {children}
    </StyledButton>
  )
}

// PropTypes
Button.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func,
  style: PropTypes.object,
  disabled: PropTypes.bool
}

export default Button
