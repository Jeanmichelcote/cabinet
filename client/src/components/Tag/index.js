import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Dismiss from 'components/Dismiss'

const Value = styled.span`
  font-size: 0.7rem;
  padding: 0.1rem 0.1rem 0.1rem 0.3rem;
  border: 1px solid #99b;
  border-radius: 2px;
  margin-left: 0.3rem;
  background-color: white;


  &:hover {
    cursor: pointer;
    color: white;
    background-color: #88a;
  }
`

const Tag = ({ _id, value, handleClick }) => (
  <Value>{value}
    <Dismiss
      width="0.9rem"
      height="0.9rem"
      handleClick={handleClick}
      id={_id}
    />
  </Value>
)

Tag.displayName = 'Tag'
Tag.propTypes = {
  id: PropTypes.string,
  value: PropTypes.string.isRequired,
  handleClick: PropTypes.func
}

export default Tag;
