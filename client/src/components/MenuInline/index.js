import React from 'react'
import styled from 'styled-components'
import {v4} from 'uuid'

const Ul = styled.ul`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  align-items: end;
  list-style-type: none;
  padding: 0;
  margin: 0;
  width: 100%;
`

const MenuInline = ({ children }) => (
  <nav>
    <Ul>
      { children.map(item => <li key={v4()}>{item}</li>) }
    </Ul>
  </nav>       
)


MenuInline.displayName = 'MenuInline';

export default MenuInline;
