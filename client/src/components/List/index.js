import React from 'react';
import styled from 'styled-components'
import {v4} from 'uuid'

// Styles
const Row = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  width: 100%;
`

const Span = styled.span`
  text-align: ${props => props.ix % 2 === 0 ? 'left' : 'right'};
  font-variant: ${props => props.ix % 2 === 0 ? 'small-caps' : 'none'};
  font-size: ${props => props.ix % 2 === 0 ? '0.8rem' : '0.9rem'};
  overflow: hidden;
  text-overflow: ellipsis;
`

// Component
const List = ({ children }) => (
  <Row>
    { children.map((item, ix) => <Span key={v4()} ix={ix}>{item}</Span> ) }
  </Row>      
)

List.displayName = 'List';

export default List;
