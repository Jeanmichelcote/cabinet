import React, {Component} from 'react';
import PropTypes from 'prop-types'
import styled from 'styled-components';

// Styles
const Input = styled.input`
  font-size: 2rem;
  width:50%;
  text-align: center;
  color: #969696;
  opacity: 0.5;
  transition: opactity 0.1s;

  &:focus {
    outline: none;
    opacity: 1;
  }
`;

// Component
class InputField extends Component {
  static displayName = 'InputField';
  static propTypes = {
    type: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
  }

  constructor(props) {
    super(props);
    this.state = {name: ''};
  }

  handleTyping = event => { this.setState({name: event.target.value}) }

  render() {
    return (
        <Input
          value={this.state.name}
          onChange={this.handleTyping}
          {...this.props}
        />
    );
  }
};

export default InputField;
