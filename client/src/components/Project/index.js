import React from 'react'
import styled from 'styled-components'
import Icon from 'react-icons/lib/md/add';
import MetadataParser from 'containers/MetadataParser'

// Styles
const Article = styled.article`
  display: grid;
  grid-template-columns: 300px 200px 1fr;
  grid-column-gap: 1rem;
  align-items: start;
  justify-items: start;
  height: 200px;
  width: ${props => props.width - 90}px;
  padding: 0.5rem 0 0.5rem 0.5rem;
  margin-bottom: 1rem;
  border-radius: 3px 0 0 3px;
  border: 1px solid ${props => props.ix % 2 === 0 ? '#ddd' : '#fafafa'};
  background-color: ${props => props.ix % 2 === 0 ? "#f3f3f3" : "transparent"};
  transition: border 0.1s ease, background-color 0.1s ease;

  &:hover {
    border: 1px solid #aaa;
    background-color: #e3e3e7;
  }
`

const Frame = styled.div`
  width: ${props => props.width - 623}px;
  height: 100%;
  overflow-x: scroll;
  overflow-y: hidden;
  border-left: 1px solid #ddd;
`

const Slider = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, 200px);
  grid-column-gap: 1rem;
  padding: 0;
  margin: 0;
  width: 2000px;
  height: 100%;
  xborder-radius: 3px;
  xborder: 1px solid #ddd;
  
`

const Image = styled.div`
  background-color: white;
`

const AddFigureIcon = styled(Icon)`
  cursor: pointer;
  align-self: center;
  justify-self: center;
  xpadding: 1rem;
  opacity: 0.5;

  &:hover {
    opacity: 0.9;
  }
`;

// Comonent
const Project = ({projectData, width, ix}) => (
  <Article ix={ix} width={width}>
    <MetadataParser projectData={projectData} />
    <AddFigureIcon 
      height="11rem"
      width="11rem"
      color="#88a"
      onClick={() => console.log(`Add new Figure to project ${projectData._id}`)}
    />
    <Frame width={width}>
      <Slider>
        <Image></Image>
        <Image></Image>
        <Image></Image>
        <Image></Image>
        <Image></Image>
        <Image></Image>
        <Image></Image>
        <Image></Image>
        <Image></Image>
      </Slider>
    </Frame>
  </Article>    
)

Project.displayName = 'Project';

export default Project;
