/**
*
* Field
*
*/

import React from 'react';
import PropTypes from 'prop-types'
import styled from 'styled-components';

import Button from 'components/Button';
import InputField from 'components/InputField';
import DownArrowIcon from 'react-icons/lib/go/arrow-down';
import SaveIcon from 'react-icons/lib/md/save';
import CancelIcon from 'react-icons/lib/fa/close';

const Form = styled.form`
  display: grid;
  grid-template-rows: repeat(3, 1fr);
  grid-row-gap: 1rem;
  justify-items: center;
  align-items: center;
  width: 100%;
  font-size: 1.2rem;
`

const Controls = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-column-gap: 1rem;
  justify-items: center;
  width: 150px;
`

function AddProjectForm({handleSave, handleCancel}) {
  return (
    <Form action="" onSubmit={handleSave}>
      <DownArrowIcon color='#88a' width='2rem' height='2rem'/>
      <InputField
        type="text"
        name="newProject"
        placeholder="Name your new project"
        autoFocus
        onKeyUp={e => {
          e.keyCode === 13 && e.target.value && handleSave(e.target.value);
          e.keyCode === 27 && handleCancel();
        }}
      />

      <Controls>
        <Button label="save"  type="submit">
          <SaveIcon width='1.2rem' height='1.2rem' />
        </Button>

        <Button label="cancel" onClick={() => handleCancel()}>
          <CancelIcon width='1.2rem' height='1.2rem' color='#a44' />
        </Button>
      </Controls>
    </Form>
  );
}

AddProjectForm.propTypes = {
  handleSave: PropTypes.func.isRequired,
  handleCancel: PropTypes.func.isRequired
};

export default AddProjectForm;
