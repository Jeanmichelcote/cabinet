import React from 'react'
import styled from 'styled-components'
import MainNav from './MainNav'
import Search from './Search'

// Styles
const HeaderFrame = styled.header`
  display: grid;
  grid-template-columns: 200px auto 50px;
  grid-column-gap: 1rem;
  align-items: end;
  border-bottom: 1px solid #88a;
  position: fixed;
  top: 0;
  height: 3em;
  width: 99%;
  background-color: white;
  z-index: 10;
`

const LocaleToggle = () => {
  const style = {
    margin: 0,
    padding: 0
  }

  return <p style={style}>locale</p>
}


// Component
const Header = () => (
  <HeaderFrame>
    <MainNav />
    <Search />
    <LocaleToggle />
  </HeaderFrame>
)

export default Header
