import React from 'react'
import styled from 'styled-components'
import { NavLink } from 'react-router-dom'
import MenuInline from 'components/MenuInline'

// Styles
const StyledLink = styled(NavLink)`
  text-decoration: none;
  color: #88a;
  opacity: 0.6;

  &:hover {
  opacity: 1;
  }
`;

const activeStyle = {
  opacity: 1,
  borderBottom: '3px solid #88a'
}

// Component
const MainNav = () => (
  <MenuInline>
    <StyledLink to="/" exact activeStyle={activeStyle}>
      Projects
    </StyledLink>

    <StyledLink to="/library" activeStyle={activeStyle}>
      Library
    </StyledLink>
  </MenuInline>
);

export default MainNav
