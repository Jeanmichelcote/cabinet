import React from 'react'
import ReactDOM from 'react-dom'
import Header from './index'
import MainNav from './MainNav'
import Search from './Search'
import {BrowserRouter} from 'react-router-dom'

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<BrowserRouter><Header /></BrowserRouter>, div);
})

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<BrowserRouter><MainNav /></BrowserRouter>, div);
})

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<BrowserRouter><Search /></BrowserRouter>, div);
})