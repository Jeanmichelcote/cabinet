import React from 'react';
import styled from 'styled-components';
import SearchIcon from 'react-icons/lib/md/search';

// Styles
const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 40px auto;
  align-items: end;
  xheight: 100%;
`;

const Form = styled.form`
  width: 100%;
`;

const Input = styled.input`
  font-size: 1em;
  width: 100%;
  border: none;
  transition: border 0.1s;
  color: #969696;

  &:focus {
    opacity: 1;
    border-bottom: solid 2px #88a;
    outline: none;
  }
`;

// Component
const Search = () => ( 
  <Wrapper>
    <SearchIcon height="2em" width="2em" color="#88a" />
    
    <Form onSubmit={() => console.log('Searching...')}>
      <Input 
        id="query"
        type="search"
        placeholder="Search the whole store..."
      />
    </Form>
  </Wrapper>
)

export default Search
