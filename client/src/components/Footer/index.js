import React from 'react';
import styled from 'styled-components'

// Styles
const Wrapper = styled.footer`
  border-top: 1px solid #88a;
  position: fixed;
  bottom: 0;
  width: 100%;
  height: 3rem;
  background-color: white;
  z-index: 10;
`;

// Component
function Footer() {
  return (
    <Wrapper>
      <section>Footer</section>
    </Wrapper>
  );
}

export default Footer