import React from 'react';
import PropTypes from 'prop-types'
//import styled from 'styled-components'
import List from 'components/List'

const MetadataRow = ({ label, value }) => {
  return (
    <List>
      <span>{label}</span>
      <span>{value}</span>
    </List>
  )
};

MetadataRow.displayName = 'MetadataRow';

MetadataRow.propTypes = {
    className: PropTypes.string,
};

export default MetadataRow;
