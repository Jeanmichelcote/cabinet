/**
*
* SideMenu
*
*/

import React from 'react';
import PropTypes from 'prop-types'
import styled from 'styled-components';

import Button from 'components/Button'
import PlusIcon from 'react-icons/lib/md/library-add'
import ToggleOnIcon from 'react-icons/lib/fa/toggle-on'
import ToggleOffIcon from 'react-icons/lib/fa/toggle-off'
import AddFigureIcon from 'react-icons/lib/md/add-a-photo'

// Styles
const Menu = styled.aside`
  position: absolute;
  top: 35%;
  bottom: 35%;
  left: 1rem;
  width: 2.5rem;
  min-height: 100px;
  border: 1px solid rgba(136,136,170,0.2);
  background-color: rgba(136,136,170,0.1);
  border-radius: 3px;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
  xbackground-color: transparent;
`

// Own attributes
const attrs = {
  icon: {
    height: "2rem",
    width: "2rem",
    color: "#88a"
  },
  button: {
    backgroundColor: 'transparent',
    border: 'none',
    margin: 0,
    padding: 0,
    outline: 'none'
  },
  tooltip: {
    'place': 'right',
    'effect': 'solid',
    'delayShow': 500
  }
}

/* Icon renderer*/
function ToggleMetaIcon(bool, handleClick) {
  const dataProps = {
    'data-tip': '',
    'data-for': 'meta'
  }
  const props = {
    ...attrs.icon,
    ...dataProps,
    onClick: () => handleClick(),
  }

  return bool ? <ToggleOnIcon {...props} /> : <ToggleOffIcon {...props} />;
}

// Component
const SideMenu = ({showMetadata, showAddProjectForm, toggleMetadata, toggleProjectForm}) => (
  <Menu>
    {/* New Project Icon */}
    <Button
      style={attrs.button}
      onClick={() => toggleProjectForm()}
      disabled={showAddProjectForm ? true : false}
    >        
      <PlusIcon {...attrs.icon} />
    </Button>

    {/* Toggle Metadata Icon */}
    <Button style={attrs.button}>
      {ToggleMetaIcon(showMetadata, toggleMetadata)}
    </Button>

    {/* New Figure Icon */}        
    <Button style={attrs.button}>
      <AddFigureIcon
        {...attrs.icon}
        onClick={() => console.log("Add new Figure")}
      />
    </Button>
  </Menu>
)

SideMenu.displayName = "SideMenu";
SideMenu.propTypes = {
  showMetadata: PropTypes.bool.isRequired,
  toggleMetadata: PropTypes.func.isRequired,
  toggleProjectForm: PropTypes.func.isRequired,
  showAddProjectForm: PropTypes.bool.isRequired
};

export default SideMenu;
