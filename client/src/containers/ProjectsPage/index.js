/*
 *
 * ProjectsPage
 *
 */

import React, { Component } from 'react';
import styled from 'styled-components';
import {v4} from 'uuid'
import {request} from 'helpers'
import Project from 'components/Project'
import SideMenu from 'components/SideMenu'
import AddProjectForm from 'components/Project/AddProjectForm'
import apiCall from 'utils'

//
// Styled
//
const Section = styled.div`
  margin-left: 4rem;
  width: 100%;
`;

const Article = styled.article`
  position: relative;
  display: flex;
  flex-flow: row nowrap;
  justify-content: flex-start;
  xmargin: 0 0 1rem 0;
  padding: 0.5rem 0 0.5rem 0.5rem;
  height: 200px;
  width: 100%;
  border-radius: 3px 0 0 3px;
  border: 1px solid ${props => props.ix % 2 === 0 ? '#ddd' : '#fafafa'};
  background-color: ${props => props.ix % 2 === 0 ? "#f3f3f3" : "transparent"};
  transition: border 0.2s ease, background-color 0.2s ease;

  &:hover {
    border: 1px solid #aaa;
    background-color: #e3e3e7;
  }
`

const StyledArticle = styled(Article)`
  position: fixed;
  top: 35%;
  background-color: #e3e3e7;
  border: 1px solid #aaa;
  box-shadow: 0px 4px 16px -3px rgba(0,0,0,0.75);
  z-index: 9999;
`

//
// Component
//
class ProjectsPage extends Component {
  static displayName = "ProjectsPage";
  static endpoint = (id) => {
    const api_projects = '/api/projects'
    return id ? `${api_projects}/${id}` : api_projects
  }

  constructor(props) {
    super(props)
    this.state = {
      windowWidth: window.innerWidth,
      projects: [],
      showMetadata: true,
      showAddProjectForm: false
    }
  }

  /* Lifecycle methods */
  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
    this.api_getProjects()
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)  
  }

  /* State changes */
  updateDimensions = () => { this.setState({windowWidth: window.innerWidth}) } 

  toggleMetadata = () => {
    this.setState({showMetadata: !this.state.showMetadata})
  }
  
  toggleProjectForm = () => {
    this.setState({showAddProjectForm: !this.state.showAddProjectForm})
  }

  /* API Calls */
  api_getProjects = (id) => {
    request(this.constructor.endpoint(id))
      .then(projects => this.setState({projects}))
  }

  api_createProject = (event) => {
    event.preventDefault()
    this.toggleProjectForm()

    const projectName = event.target.querySelector('input').value

    request(this.constructor.endpoint(), {method: 'POST'})
      .then(project => request('api/fields', {
        method: 'POST',
        body: JSON.stringify({
          label: 'Name',
          value: projectName,
          parent: project._id
        })
      }))
  }

  api_updateProject = (id, data) => {
    request(this.constructor.endpoint(id), {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'PATCH',
      body: JSON.stringify({data})
    })
      .then(projects => this.setState({projects}))
  }

  api_deleteProject = (id) => {
    request(this.constructor.endpoint(id), {method: 'DELETE'})
      .then(() => { request(`api/fields?parents=${id}`, {method: 'DELETE'}) })
        .then(() => this.api_getProjects())
  }

  /* Render */
  render() {
    const {projects, windowWidth, showMetadata, showAddProjectForm} = this.state
    return (
      <Section>
        <SideMenu
          showMetadata={showMetadata}
          showAddProjectForm={showAddProjectForm}
          toggleMetadata={this.toggleMetadata}
          toggleProjectForm={this.toggleProjectForm}
        />
        {
          showAddProjectForm && (
            <StyledArticle>
              <AddProjectForm
                handleSave={this.api_createProject}
                handleCancel={this.toggleProjectForm}
              />
            </StyledArticle>
          )
        }
        {
          projects.map((data, ix) =>
            <Project key={v4()} ix={ix} projectData={data} width={windowWidth} />
          )
        }
      </Section>
    );
  }
}
 
export default ProjectsPage
