/*
 *
 * LibraryPage
 *
 */

import React, { Component } from 'react';
import styled from 'styled-components';
import Library from 'components/Library'

// Styled
const Main = styled.main`
  padding: 1rem 0 1em 4rem;
  position: absolute;
  top: 60px;
  bottom: 60px;
  overflow: scroll;
  width: 100%;
`;

// Component
class LibraryPage extends Component {
  static displayName = "LibraryPage";

  constructor(props) {
    super(props);
    this.state = {
      windowWidth: window.innerWidth
    }
  }

  componentDidMount() {
    window.addEventListener("resize", this.updateDimensions)
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateDimensions)  
  }

  updateDimensions = () => { this.setState({windowWidth: window.innerWidth}) } 

  render() {
    const {windowWidth} = this.state
    
    return (
      <Main>
        <Library
          windowWidth={windowWidth}
        />
      </Main>
    );
  }
}
 
export default LibraryPage
