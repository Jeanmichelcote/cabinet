import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'
import {v4} from 'uuid'
import MetadataRow from 'components/MetadataRow'
import List from 'components/List'
//import TagParser from 'containers/TagParser'
import Tag from 'components/Tag'
import {formatDate} from 'helpers'

// Styles
const Box = styled.div`
  display: grid;
  grid-template-rows: 1fr 10%;
  height: 100%;
  width: 100%;
`

const Fields = styled.div`
`

const Tags = styled.div`
  align-self: end;
  justify-self: end;
`

// Component
class MetadataParser extends Component {
  static propTypes = {
    projectData: PropTypes.shape({
      _id: PropTypes.string.isRequired,
      lastUpdatedOn: PropTypes.number.isRequired,
      createdOn: PropTypes.number.isRequired,
      isTouched: PropTypes.bool.isRequired,
      children: PropTypes.objectOf(PropTypes.arrayOf(PropTypes.string))
    })
  }

  constructor(props) {
    super(props)
    this.state = {
      fields: [],
      tags: []
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState !== this.state;
  }

  componentDidMount() {
    const {_id} = this.props.projectData
    fetch(`/api/fields?parent=${_id}`)
      .then(data => data.json())
      .then(fields => this.setState( {fields} ))

    fetch(`/api/tags?parents=${_id}`)
      .then(data => data.json())
      .then(tags => this.setState({tags}))
  }

  onClickDismiss = (tagId) => {
    const {tags} = this.state
    console.log('aaa', tags)
    const ix = tags.findIndex(tagId)
    if (ix !== -1) {
      const newTags = tags.filterNot(tag => tag === tagId)
      this.setState({tags: newTags})
    } 
  }

  render() {
    const {createdOn, lastUpdatedOn} = this.props.projectData
    const {fields, tags} = this.state
    
    return (
      <Box>
        <Fields>
          {fields.map((data, ix) => <MetadataRow key={v4()} ix={ix} {...data} />)}
          <List>
            <span>Created On</span><time>{formatDate(createdOn)}</time>
            <span>Last Updated On</span><time>{formatDate(lastUpdatedOn)}</time>
          </List>
        </Fields>
        <Tags>
          {
            tags.map((data, ix) =>
              <Tag key={v4()} ix={ix} {...data} handleClick={this.onClickDismiss} />
            )
          }
        </Tags>
      </Box>
    )
  }
}

export default MetadataParser
