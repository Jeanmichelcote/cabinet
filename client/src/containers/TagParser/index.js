import React, { Component } from 'react';
import PropTypes from 'prop-types'
import {v4} from 'uuid'

const Tag = ({value}) => {
  <span>{value}</span>
}

// Component
class TagParser extends Component {
  static propTypes = {
    parent: PropTypes.string.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      tags: []
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextState !== this.state;
  }

  componentWillMount() {
    fetch(`/api/tags?parents=${this.props.parent}`)
      .then(data => data.json())
      .then(tags => this.setState({tags}))
  }

  render() {
    console.log(';;;', this.state.tags)
    return (
      <div>{this.state.tags.map(tag => <Tag key={v4()} {...tag} />)}</div>
    );
  }
}

export default TagParser;
